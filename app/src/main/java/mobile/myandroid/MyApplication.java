package mobile.myandroid;

import android.app.Application;

/**
 * My Android
 * ${PACKAGE_NAME}
 * Created by beou on 27/10/2015.
 */
public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}
