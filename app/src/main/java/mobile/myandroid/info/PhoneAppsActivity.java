package mobile.myandroid.info;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mobile.myandroid.BaseActivity;
import mobile.myandroid.R;

/**
 * Created by beou on 26/10/2015.
 */
public class PhoneAppsActivity extends BaseActivity {
    private static PackageManager packageManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_apps);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //--
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this,
                drawer,
                toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        //--


        List<AppItem> items = null;
        try {
            items = getAppsList();
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }


        ListView listPhoneApps = (ListView) findViewById(R.id.list_phone_apps);
        PhoneAppsAdapter adapter = new PhoneAppsAdapter(this, items);
        listPhoneApps.setAdapter(adapter);

        listPhoneApps.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                AppItem item = (AppItem)parent.getItemAtPosition(position);
                openApplication(PhoneAppsActivity.this, item.getPackageName());
            }
        });
    }

    @Override
    public PackageManager getPackageManager() {
        if (packageManager == null) {
            packageManager = super.getPackageManager();
        }
        return packageManager;
    }

    private List<AppItem> getAppsList() throws PackageManager.NameNotFoundException {
        List<AppItem> listApps = new ArrayList<>();

        PackageManager pm = getPackageManager();
        List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);
        for (ApplicationInfo ai : packages) {
            String appName = pm.getApplicationLabel(ai).toString();
            Drawable appIcon = pm.getApplicationIcon(ai);
            PackageInfo packageInfo = pm.getPackageInfo(ai.packageName, PackageManager.GET_META_DATA);
            long lastUpdateTime = packageInfo.lastUpdateTime;
            Date lastUpdated = new Date(lastUpdateTime);
            AppItem item = new AppItem(appName, appIcon, lastUpdated);
            item.setPackageName(ai.packageName);
            item.setPackageInfo(packageInfo);
            listApps.add(item);
        }
        return listApps;
    }

    private void openApplication(Context context, String packageName) {
        PackageManager pm = getPackageManager();
        Intent i = pm.getLaunchIntentForPackage(packageName);
        if (i == null) {
            return;
        }
        i.addCategory(Intent.CATEGORY_LAUNCHER);
        context.startActivity(i);
        return;
    }

    private class AppItem {
        private PackageInfo packageInfo;
        private String packageName;
        private String appName;
        private Drawable appIcon;
        private Date installedDate;

        public AppItem(String appName) {
            this.appName = appName;
        }

        public AppItem(String appName, Drawable appIcon) {
            this.appName = appName;
            this.appIcon = appIcon;
        }

        public AppItem(String appName, Drawable appIcon, Date installedDate) {
            this.appName = appName;
            this.appIcon = appIcon;
            this.installedDate = installedDate;
        }

        public PackageInfo getPackageInfo() {
            return packageInfo;
        }

        public void setPackageInfo(PackageInfo packageInfo) {
            this.packageInfo = packageInfo;
        }

        public String getPackageName() {
            return packageName;
        }

        public void setPackageName(String packageName) {
            this.packageName = packageName;
        }

        public String getAppName() {
            return appName;
        }

        public void setAppName(String appName) {
            this.appName = appName;
        }

        public Drawable getAppIcon() {
            return appIcon;
        }

        public void setAppIcon(Drawable appIcon) {
            this.appIcon = appIcon;
        }

        public Date getInstalledDate() {
            return installedDate;
        }

        public void setInstalledDate(Date installedDate) {
            this.installedDate = installedDate;
        }
    }
    private class PhoneAppsAdapter extends BaseAdapter {

        private LayoutInflater mInflater;
        private List<AppItem> items;

        public PhoneAppsAdapter(Context context, List<AppItem> items) {
            this.mInflater = LayoutInflater.from(context);
            this.items = items;
        }

        @Override
        public int getCount() {
            return items != null ? items.size() : 0;
        }

        @Override
        public AppItem getItem(int position) {
            return items != null ? items.get(position) : null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            AppViewHolder holder = null;
            if (convertView == null) {
                holder = new AppViewHolder();
                convertView = mInflater.inflate(R.layout.list_item_phone_app, null);

                holder.appIcon = (ImageView) convertView.findViewById(R.id.img_app_icon);
                holder.appName =  (TextView) convertView.findViewById(R.id.txt_app_name);
                holder.appInstalled = (TextView) convertView.findViewById(R.id.txt_app_installed);
                holder.btnAppDel = (ImageButton) convertView.findViewById(R.id.btn_app_del);
                convertView.setTag(holder);
            } else {
                holder = (AppViewHolder)convertView.getTag();
            }

            final AppItem item = (AppItem) getItem(position);

            holder.appIcon.setImageDrawable(item.getAppIcon());
            holder.appName.setText(item.getAppName());
            holder.appInstalled.setText(item.getInstalledDate().toString());
            holder.btnAppDel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //-- show a dialogAlert
                    //-- delete app
                    uninstallApp(item);
                }
            });

            return convertView;
        }
    }

    private void uninstallApp(AppItem item) {
        if (isSystemPackage(item.getPackageInfo())) {
            Toast.makeText(this, getString(R.string.cannot_remove_cause_system_app), Toast.LENGTH_LONG).show();
        } else {
            //Uninstall app here
            Uri packageURI = Uri.parse("package:" + item.getPackageName());
            Intent uninstallIntent = new Intent(Intent.ACTION_DELETE, packageURI);
            startActivity(uninstallIntent);
        }
    }
    private boolean isSystemPackage(PackageInfo pkgInfo) {
        return (pkgInfo.applicationInfo.flags &
                ApplicationInfo.FLAG_SYSTEM) != 0;
    }

    private class AppViewHolder {
        ImageView appIcon;
        TextView appName;
        TextView appInstalled;
        ImageButton btnAppDel;
    }
}
